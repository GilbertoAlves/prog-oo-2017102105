/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unicarioca;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Scanner;

public class ArrayPessoas {
     static ArrayList<Pessoa> list = new ArrayList<Pessoa>();
     
    public static void insert()
    {
        Scanner s = new Scanner(System.in);
        int idade;
        String nome;
        
        System.out.println("Digite nome e idade");
        nome  = s.next();
        idade = s.nextInt();
        
        
        Pessoa p = new Pessoa(nome,idade);
        list.add(p);
       
    }
    public static void list()
    {
        if(list.size() == 0)
        {
            System.out.println("N�o h� pessoas na lista.");
        }
            
        for (Pessoa pessoa : list) 
        {
            System.out.println("Pessoa "+(list.indexOf(pessoa) + 1) );
            System.out.println("Nome: "+ pessoa.getNome());
            System.out.println("Idade: "+pessoa.getIdade() + "\n");
        }
        System.out.println("\n");
    }
    public static void main(String[] args) throws InterruptedException, IOException 
    {
        while (true)
        {      
            Scanner s = new Scanner(System.in);
            System.out.println("1 - Inserir; \n 2 - Listar \n 3 - Sair.");
            int choose = s.nextInt();
            
            switch(choose)
            {
                case 1: 
                    insert();
                    break;
                case 2: 
                    list();
                    break;
                case 3:
                    return;
                default:
                    System.out.println("Escolha incorreta, tente novamente");
                    Thread.sleep(1000);
                    System.out.println("\n");
                    
                    
            }
        }
    }
}
