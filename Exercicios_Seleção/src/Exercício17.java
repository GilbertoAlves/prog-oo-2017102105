import java.util.Scanner;


public class Exerc�cio17 {
	static Scanner s  = new Scanner(System.in);
	public static int op(int n1, int n2) 
	{
		String op;
		op = s.next();
		
		switch(op) 
		{
			case "+":
				{
					
					return n1+n2;
				}
			case "-":
				{
					return n1-n2;
				}
			case "*":
				{
				 return n1*n2;
				}
			case "/":
				{
					if(n2 != 0)
						return n1/n2;
					else
					{
						System.out.println("Divis�o por zero proibida! Saindo do programa");
						System.exit(-1);
					}
				}
		}
		return 0;
	}
	public static void main(String[] args) 
	{
		
		
		int num1, num2;
		
		System.out.println("Digite dois n�mero");
		num1 = s.nextInt();
		num2 = s.nextInt();
		
		System.out.println("Digite a opera��o: \n +: Adi��o; \n -:Subtra��o \n *:Multiplica��o \n /:Divis�o");
	
		
		System.out.println("Resultado da opera��o: "+op(num1, num2));
		

	}
}
