

import java.util.Scanner;

public class Exerc�cio14 
{
	public static void vencedor(double g_gr, double g_in) 
	{
		if(g_gr == g_in)
			System.out.println("Grenal deu empate.");
		else 
		{
			if(g_gr > g_in)
				System.out.println("O vencedor � o Gr�mio.");
			else
				System.out.println("O vencedor � o Inter.");
		}
	}
	public static void main(String[] args) 
	{
		int gols_gremio, gols_inter;
		Scanner s = new Scanner(System.in);
		
		System.out.println("----------GRENAL------------");
		
		System.out.println("Digite o n�mero de gols do Gr�mio");
		gols_gremio = s.nextInt();
		
		System.out.println("Digite o n�mero de gols do Inter");
		gols_inter = s.nextInt();
		
		vencedor(gols_gremio, gols_inter);
	}
}
