
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2017102105
 */
public class Exercício15 
{
    public static int[] compara(int[] x, int[] y)
    {
        int maior = x.length > y.length? x.length: y.length;
        int menor = x.length < y.length? x.length: y.length;
        
        int[] _maior =  x.length > y.length? x: y;
        int[] _menor =  x.length < y.length? x: y;
        
        int num_iguais = 0;
       
        for(int i = 0; i < maior;i++)
        {
            for (int j = 0; j < menor ; j++) 
            {
                if(_maior[i] == _menor[j])
                {
                    num_iguais++;
                }
                    
            }
        }
        int[] z = new int[num_iguais];
        int k = 0;
        for(int i = 0; i < maior;i++)
        {
            for (int j = 0; j < menor ; j++) 
            {
                if(_maior[i] == _menor[j])
                {
                    z[k] =  _menor[j];
                    k++;
                }
                    
            }
        }
        
        
        return z;
    }
    public static int[] ler (int i)
    {
        Scanner s = new Scanner(System.in);
        int[] x = new int[i];
        for(int j = 0; j < i; j++)
        {
            System.out.println("Digite um número \n");    
            x[j] = s.nextInt();
        }
        return x;
    }
    public static void escrever(int[] x)
    {
        for (int i = 0; i < x.length; i++) 
        {
            System.out.println(x[i] + "\n");
        }
    }
    public static void main(String[] args) 
    {
        
        int[] r = ler(5);
        int[] t = ler(10);
        
        int[] x = compara(r,t);
        
        System.out.println("Números iguais aos dois arrays: ");
        escrever(x);
        
            
    }
}
