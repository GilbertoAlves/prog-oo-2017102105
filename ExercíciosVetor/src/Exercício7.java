import java.util.Scanner;

public class Exercício7
{
	public static void main(String[] args)
	{
	    int[] x,y = new int[10];
	    Scanner s  = new Scanner(System.in);
	    System.out.println("Digite dez números \n"); 
	    for (int i = 0; i < 10; i++)
	    {
	        x[i] = s.nextInt();
	        if (i%2 == 0)
	            y[i] = x[i]/2;
	        else
	            y[i] = x[i] * 3;
	    }
	    
	    System.out.println("Lista original: \n");
	    for(int i = 0 ;i < 10; i++)
	        System.out.println(x[i] + "\n");
	    System.out.println("Lista modificada: \n");
	    for(int i = 0 ;i < 10; i++)
	        System.out.println(y[i] + "\n");
	   
	}
}
