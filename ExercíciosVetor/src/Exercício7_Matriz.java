
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2017102105
 */
public class Exercício7_Matriz 
{
     public static int[][] ler_matriz (int i, int j)
    {
        Scanner s = new Scanner(System.in);
        int[][] x = new int[i][j];
        for(int k = 0; k < i; k++)
        {
            for(int l = 0 ; l < j ; l++)
            {
                System.out.println("Digite um número \n");    
                x[k][l] = s.nextInt();
            }
        }
        return x;
    }
    public static int ler_numero ()
    {
        Scanner s = new Scanner(System.in);
        int x;
       
        System.out.println("Digite um número \n");    
        x = s.nextInt();
      
        return x;
    }
     public static int[][] mul_diagonal(int[][] x, int p)
     {
         for (int i = 0; i < x.length; i++) 
         {
             for (int j = 0; j < x[i].length; j++)
             {
                if(i==j)
                    x[i][j] = x[i][j] * p;
             }
         }
         return x;
     }
    public static void escrever(int[][] x)
    {
        for (int i = 0; i < x.length; i++) 
        {
            for (int j = 0; j < x[i].length; j++) {
                System.out.println(x[i][j] + "\n");
            }
            
        }
    }
     public static void main(String[] args) 
     {
         int[][] matriz = ler_matriz(4, 4);
         int pivo = ler_numero();
         int[][] matriz_mul = mul_diagonal(matriz, pivo);
         
         System.out.println("Matriz com o a diagonal multiplicada por um pivô");
         
     }
}
