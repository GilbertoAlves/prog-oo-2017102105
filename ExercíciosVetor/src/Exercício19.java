
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2017102105
 */
public class Exercício19 {
    public static int[] ler (int i)
    {
        Scanner s = new Scanner(System.in);
        int[] x = new int[i];
        for(int j = 0; j < i; j++)
        {
            System.out.println("Digite um número \n");    
            x[j] = s.nextInt();
        }
        return x;
    }
    public static void escrever(int[] x)
    {
        for (int i = 0; i < x.length; i++) 
        {
            System.out.println(x[i] + "\n");
        }
    }
    
    public static int media(int[] x)
    {
        int media = 0;
        for (int i = 0; i < x.length; i++)
        {
            media = media+x[i];
      
        }
        return media/x.length;
    }
    public static int[] menores(int[] x, int num_x, int media)
    {
        int num_menores = 0;
        for (int i = 0; i < x.length; i++)
        {
            if(x[i] < media)
                num_menores++;
        }
        int[] menores = new int[num_menores];
        int j = 0;
        for (int i = 0; i < x.length; i++)
        {
            if(x[i] < media)
            {
                menores[j] = x[i];
                j++;
            }
      
        }
        return menores;
    }
     public static int[] maiores(int[] x, int num_x, int media)
    {
        int num_maiores = 0;
        for (int i = 0; i < x.length; i++)
        {
            if(x[i] < media)
                num_maiores++;
        }
        int[] maiores = new int[num_maiores];
        int j = 0;
        for (int i = 0; i < x.length; i++)
        {
            if(x[i] < media)
            {
                maiores[j] = x[i];
                j++;
            }
      
        }
        return maiores;
    }
    public static void main(String[] args) 
    {
        int[] x = ler(10);
        int[] a = maiores(x, 10, media(x));
        int[] b = menores(x, 10, media(x));
        
        System.out.println("Media do array: "+ media(x));
        
        System.out.println("Números maiores que a média: ");
        escrever(a);
        
        System.out.println("Números menores que a média: ");
        escrever(b);
    }
}
