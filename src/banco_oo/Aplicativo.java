package banco_oo;

import java.util.Scanner;

/**
 * @author Jorge Doria
 */
public class Aplicativo {
    
    //Cria o banco
    static Banco banco = new Banco();
    
    //Objeto para ler informações do teclado
    static Scanner tecla = new Scanner(System.in);
    
    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("::: MENU PRINCIPAL :::");
            System.out.println("1-Cadastrar Conta");
            System.out.println("2-Excluir Conta");
            System.out.println("3-Saldo da Conta");
            System.out.println("4-Debitar");
            System.out.println("5-Creditar");
            System.out.println("6-Transferir");
            System.out.println("7-Imprimir o saldo das Contas");
            System.out.println("8-Sair");
            System.out.println("Digite sua opção: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirConta(); break;
                case 2: excluirConta(); break;
                case 3: imprirSaldoDaConta(); break;
                case 4: debitar(); break;
                case 5: creditar(); break;
                case 6: transferir(); break;
                case 7: imprimirSaldoDasContas(); break;
                case 8: break;
            }
        } while (op!=8);
    }
    
    public static void incluirConta(){
        //Entrada de dados
        System.out.println("Digite o número da conta:");
        long num = tecla.nextLong();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        //Cadastro da conta no banco
        banco.cadastrar(new Conta(num, saldo));
    }
    
    public static void excluirConta(){
        System.out.println("Digite o número da conta:");
        long num = tecla.nextLong();
        banco.excluir(num);
    }
    
    public static void imprirSaldoDaConta(){
        //Entrada de dados
        System.out.println("Digite o número da conta:");
        long num = tecla.nextLong();
        //Imprime o saldo da conta informada
        banco.printSaldo(num);
    }
    
    public static void debitar(){
        //Entrada de dados
        System.out.println("Digite o número da conta:");
        long num = tecla.nextLong();
        System.out.println("Digite o valor do débito:");
        double valor = tecla.nextDouble();
        //Realiza o débito na conta informada
        banco.debitar(num, valor);
    }
    
    public static void creditar(){
        //Entrada de dados
        System.out.println("Digite o número da conta:");
        long num = tecla.nextLong();
        System.out.println("Digite o valor do débito:");
        double valor = tecla.nextDouble();
        //Realiza o crédito na conta informada
        banco.creditar(num, valor);
    }
    
    public static void transferir(){
        System.out.println("Digite o número da conta (ORIGEM):");
        long numOrigem = tecla.nextLong();
        System.out.println("Digite o número da conta (DESTINO):");
        long numDestino = tecla.nextLong();
        System.out.println("Digite o valor da transferência:");
        double valor = tecla.nextDouble();
        //Realiza a transferência entre as contas
        banco.transferir(numOrigem, numDestino, valor);
    }
    
    public static void imprimirSaldoDasContas(){
        banco.getSaldoTotal();
    }
    
}