package banco_oo;

/**
 * Esta classe modela um banco, com suas contas e as operações sobre elas.
 * @author Jorge Doria
 */
public class Banco {

    // O número de contas atualmente cadastradas.
    private static int indice;

    // As contas cadastradas neste banco.
    private Conta[] contas;

    // O número máximo de contas que podem ser cadastradas neste banco.
    private final int MAX_CONTAS = 5;

    /**
     * Cria um novo banco.
     */
    public Banco () {
        contas = new Conta[MAX_CONTAS];
        indice = 0;
    }

    /**
     * Cadastra uma conta no banco. Avisa se não pode efetuar o cadastro.
     */
    public void cadastrar(Conta c) {
        if (indice < MAX_CONTAS) { // Ainda pode ser cadastrada ?
            contas[indice] = c;
            indice = indice + 1;
            System.out.println("Conta cadastrada com sucesso.");
        } else {
            System.out.println("Esta agencia está cheia.Conta não cadastrada.");
        }
    }
    
    /**
     * Excluir a conta no banco. Avisa se não encontrar a conta.
     * @param numero 
     */
    public void excluir(long numero){
        Conta c;
        c = procurar(numero);
        if (c == null)
            System.out.println("A conta " + numero + " nao existe!");
        else{
            int i = 0;
            for (i = 0; i < indice; i++){
                if (contas[i].getNumero() == numero){
                    //Adiciona um objeto com atributos zerados no Array
                    //O Array não pode ficar sem referencia (null), causa uma EXCEPTION
                    contas[i] = new Conta(0, 0); 
                    System.out.println("Conta excluída com sucesso.");
                }
            }
        }
    }

    /**
     * Procura uma conta neste banco, dada o número da conta procurada.
     */
    private Conta procurar(long numero) {
        int i = 0; // Variáveis locais ao método.
        boolean achou = false;
        Conta retorno;

        while ((!achou) & (i < indice)) {
            if (contas[i].getNumero() == numero)
                achou = true;
            else
                i = i + 1;
        }

        if (achou == true)
            retorno = contas[i];
        else
            retorno = null;

        return retorno; // return tem que ser a ultima instrucao!
    }

    /**
     * Imprime os saldos da conta especificada.
     * @param num O número da conta que se deseja imprimir o saldo.
     */
    public void printSaldo(long numero) {
        Conta c;
        c = procurar(numero);
        if (c == null)
            System.out.println("A conta " + numero + " nao existe!");
        else
            System.out.println("O saldo da conta " + numero +
                               " é: " + c.getSaldo());
    }

    /**
     * Debita o valor especificado na conta dada.
     */
    public void debitar(long numero, double valor) {
        Conta c;
        c = this.procurar(numero);
        if (c == null)
            System.out.println("A conta " + numero + " nao existe!");
        else{
            c.debitar(valor);
            System.out.println("Débito realizado com sucesso.");
        }
    }

    /**
     * Credita o valor especificado na conta dada.
     */
    public void creditar(long numero, double valor) {
        Conta c;

        c = this.procurar(numero);
        if (c == null)
            System.out.println("A conta " + numero + " nao existe!");
        else{
            c.creditar(valor);
            System.out.println("Crédito realizado com sucesso.");
        }
    }

   /**
     * Transfere um valor especificado entre duas contas.
     *
     * @param numDe   O número da conta a ser debitada.
     * @param numPara O número da conta a ser creditada.
     * @param val     O valor a ser transferido.
     */
    public void transferir(long numeroDe, long numeroPara, double valor) {
        Conta contaDe, contaPara;

        contaDe = this.procurar(numeroDe);
        contaPara = this.procurar(numeroPara);
        if ((contaDe == null) || (contaPara == null))
            System.out.println("Um das contas não existe!");
        else {
            contaDe.debitar(valor);
            contaPara.creditar(valor);
            System.out.println("Transferência realizada com sucesso.");
        }
    }

    /**
     * Imprime a soma dos saldos de todas as contas do banco.
     */
    public void getSaldoTotal() {
        int i;
        double total;
        total = 0.0;
        for (i = 0; i < indice; i++)
            total = total + contas[i].getSaldo();
        System.out.println("O saldo total das contas é: " + total);
    }

    /**
     * Retorna o número de clientes do banco.
     */
    public int getNumeroClientes() {
        return indice;
    }
}